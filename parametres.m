%% Paramètres de simulation
% G. Jodin, J. Scheller, E. Duhayon, J. F. Rouchon, M. Triantafyllou, M. Braza. “An Experimental Platform for Surface Embedded SMAs in Morphing Applications”, Solid State Phenomena, Volume 260, pages 69-76, 2017, doi.org/10.4028/www.scientific.net/SSP.260.69.

clear all;
close all;
clc;

%% Caractéristiques matériaux

matpars.E_A = 67e9;   % [Pa]
matpars.E_M = 26.3e9;   % [Pa]]

matpars.C_A = 13.8e6;   % [Pa]
matpars.C_M = 8e6;      % [Pa]

matpars.A_s = 40;     % [°C]
matpars.A_f = 65.0;     % [°C]

matpars.M_s = 50;        % [°C]
matpars.M_f = 20;     % [°C]
matpars.eps_L = 0.1;  % [ ]

matpars.theta = 0; % [ ]

% Précontrainte du brin d'AMF
r = 1e-3; % [m] rayon du brin
l0 = 10e-2; % [m] longueur au repos

matpars.A_c = pi*2*r*l0;% [m²] surface de fil
matpars.A = pi*r^2; % section du fil d'AMF
sig0 = 0;% [Pa] précontrainte
eps0 = 3e-2; % [1] préalongement
xi0 = 0.9; % [1] fraction martensitique initiale

matpars.R = 1;% [Ohm] résitance du brin d'AMF
matpars.m = 6500*matpars.A*l0;% Masse linéique.
matpars.c_p = 320;% [J/(kg.°C)] chaleur spécifique
matpars.h_c = 120;% [W/(m².°C)] convection


%% Geométrie et plaque
a = 2.5e-3; % [m] entraxe de l'AMF sur la plaque
L = 200e-3;% [m] longueur active plaque

E = 70e9;% [GPa] module d'Young de la plaque
Ig = 35e-3*(1e-3)^3/12;% [m⁴] moment quadratique section plaque
k = (2*E*Ig)/(a*L^2);% suivant équation de poutre de Bernouilli

%% Conditions expérimentales
I = 8;% [A] amplitude de courant
T_inf = 20;% [°C] température ambiante


busInfo = Simulink.Bus.createObject(matpars);